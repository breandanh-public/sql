#!/bin/bash
clear;

GPG_CHECKFILE=gpgcheckfile

# Remove Previously Signed Files
find ./ -name '*.asc' | xargs rm


## Sign Files
#find ./src -name '*.java' | xargs gpg -ab

## Check Signing
#find ./src -name '*.asc' | xargs gpg --verify

## Sign JAR
#find ./target -name '*.jar' | xargs gpg -ab

## Check Signed JAR
#find ./target -name '*.asc' | xargs gpg --verify

# Sign Checksum
gpg -ab $GPG_CHECKFILE

# Check Signed Checksum
gpg --verify $GPG_CHECKFILE.asc
