package uk.co.breandanh.SQL;

import java.io.File;
import java.io.FileNotFoundException;
import java.net.URL;
import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.*;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.spy;
import static org.mockito.Mockito.when;

public class PreparedStatementTest{
    
    private static final String query =
            "SELECT "
            + "username, "
            + "email "
            + "FROM "
            + "users "
            + "WHERE "
            + "username=? "
            + "AND "
            + "password=?";
    private static PreparedStatement classUnderTest;
    
    @Before
    public void setUp() throws Exception{
        classUnderTest = new PreparedStatement(query);
    }
    
    @Test
    public void setupThrowsNullPointerException() throws PreparedStatement.OriginalQueryException{
        ClassLoader mockClassLoader = mock(ClassLoader.class);
        when(mockClassLoader.getResource(any(String.class))).thenReturn(null);
        classUnderTest = new MockedPreparedStatement(mockClassLoader);
    }
    
    @Test
    public void setupThrowsIOException() throws PreparedStatement.OriginalQueryException{
        URL mockURL = mock(URL.class);
        when(mockURL.getFile()).thenThrow(new ArrayIndexOutOfBoundsException());
        ClassLoader mockClassLoader = mock(ClassLoader.class);
        when(mockClassLoader.getResource(any(String.class))).thenReturn(mockURL);
        classUnderTest = new MockedPreparedStatement(mockClassLoader);
    }
    
    @Test
    public void readFileThrowsIOException() {
        String fileName = "ThisFileDoesNotExist.txt";
        File file = new File(fileName);
        File mockFile = spy(file);
        when(mockFile.exists()).thenReturn(false);
        try{
            classUnderTest.readFile(mockFile);
            fail("Exception Should Have Been Thrown");
        }catch(FileNotFoundException e){
            assertEquals(
                    fileName + " (No such file or directory)",
                    e.getMessage()
            );
        }
    }
    
    @Test
    public void invalidVariableThrowsException(){
        try{
            classUnderTest.prepare(new InvalidObject(), "");
            fail("Exception Should Have Been Thrown");
        }catch(PreparedStatement.InvalidValuesException e){
            assertEquals("Invalid Class", e.getMessage());
        }
    }
    
    @Test
    public void incorrectNumberOfVariablesReturnsEmptyString() throws PreparedStatement.InvalidValuesException{
        String preparedStatement = classUnderTest.prepare("");
        assertEquals("", preparedStatement);
    }
    
    @Test
    public void sqlInjectionThrowsException(){
        String sqlInjectionQuery = "' OR 1=1 -- ";
        try{
            classUnderTest.prepare(sqlInjectionQuery, "");
            fail("Exception Should Have Been Thrown");
        }catch(PreparedStatement.InvalidValuesException e){
            assertEquals(
                    "SQL Syntax Error: Found reserved words in provided value"
                    + "\n\t[QUERY]: " + query
                    + "\n\t[VALUE]: '" + sqlInjectionQuery + "'"
                    + "\n\n",
                    e.getMessage()
            );
        }
    }
    
    @Test
    public void sqlInjectionReturnsEscapedValues() throws PreparedStatement.InvalidValuesException{
        String sqlInjectionQuery = "' -- ";
        String expectedReturnedString = query.replaceAll("\\?", "'" + "\\\\" + "\\'" + "'");
        assertEquals(expectedReturnedString, classUnderTest.prepare(sqlInjectionQuery, sqlInjectionQuery));
    }
    
    @Test
    public void originalQueryThrowsSyntaxException(){
        String badQuery = "SELECT * FROMusers;";
        try{
            classUnderTest = new PreparedStatement(badQuery);
            fail("Exception Should Have Been Thrown");
        }catch(PreparedStatement.OriginalQueryException e){
            String expectedError = "Failed to Parse SQL: null";
            assertEquals(
                    expectedError,
                    e.getMessage().substring(0, expectedError.length())
            );
        }
    }
    
    private class InvalidObject{
    }
    
    private class MockedPreparedStatement extends PreparedStatement{
        private MockedPreparedStatement(ClassLoader classLoader) throws OriginalQueryException{
            super(classLoader, query);
        }
    }
}