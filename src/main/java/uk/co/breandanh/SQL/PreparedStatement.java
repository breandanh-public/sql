package uk.co.breandanh.SQL;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.StringReader;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Scanner;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import net.sf.jsqlparser.JSQLParserException;
import net.sf.jsqlparser.parser.CCJSqlParserManager;
import net.sf.jsqlparser.parser.JSqlParser;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.util.StringUtils;

public class PreparedStatement{
    private static final Logger LOGGER = LoggerFactory.getLogger(PreparedStatement.class);
    private final List<Class> allowedClasses = Arrays.asList(
            Double.class,
            Float.class,
            Integer.class,
            String.class
    );
    private final List<String> prohibitedChars = Arrays.asList(
            "\\-\\-",
            "\\\\",
            "\\*"
    );
    private final List<String> charsToReplace = Arrays.asList(
            "\\'",
            "\\%",
            "\\#",
            "\\;"
    );
    private final List<String> prohibitedKeywords = new ArrayList<>();
    private final ClassLoader classLoader;
    private List<String> allowedKeywords = new ArrayList<>();
    private String query = "";
    
    /**
     * PreparedStatement Constructor.
     * @param originalQuery The original query string (with '?' as placeholders for values)
     */
    public PreparedStatement(final String originalQuery) throws OriginalQueryException{
        this.classLoader = this.getClass().getClassLoader();
        this.init(originalQuery);
    }
    
    /**
     * PreparedStatement Constructor.
     * @param originalQuery The original query string (with '?' as placeholders for values)
     */
    PreparedStatement(
            ClassLoader classLoader,
            String originalQuery
    ) throws OriginalQueryException{
        this.classLoader = classLoader;
        this.init(originalQuery);
    }
    
    private void init(String originalQuery) throws OriginalQueryException{
        this.populateReservedWords();
        this.validateSQLSyntax(originalQuery);
        this.setQuery(originalQuery);
    }
    
    /**
     * Prepares the query with the provided values.
     * @param values An array of Object csv
     * @return The populated prepared query
     */
    public String prepare(Object... values) throws InvalidValuesException{
        if(this.validateValues(values)){
            String[] escapedStrings = this.escapeObjects(values);
            String preparedStatement = this.replacePlaceholders(escapedStrings);
            if(this.compareOriginalQueryandPreparedStatement(this.getQuery(), preparedStatement, escapedStrings)){
                return preparedStatement;
            }
        }
        return "";
    }
    
    private boolean compareOriginalQueryandPreparedStatement(
            String originalQuery,
            String preparedStatement,
            String[] escapedStrings
    ){
        StringBuilder splitOriginalQuery = new StringBuilder();
        Arrays.stream(originalQuery.split("\\?")).forEach(
                origQuery -> splitOriginalQuery.append(
                        origQuery.trim()
                )
        );
        
        StringBuilder splitPreparedStatement = new StringBuilder();
        StringBuilder stringToTest = new StringBuilder(preparedStatement);
        for(String escapedString : escapedStrings){
            String[] splitTestString = stringToTest.toString().split("\\'" + escapedString + "\\'");
            splitPreparedStatement.append(splitTestString[0].trim());
            stringToTest = new StringBuilder();
            for(int i = 1; i < splitTestString.length; i++){
                stringToTest.append(splitTestString[i]);
            }
        }
        
        return splitOriginalQuery.toString()
                .equalsIgnoreCase(
                        splitPreparedStatement.toString()
                )
                && splitOriginalQuery.toString()
                .trim().length() == splitPreparedStatement.toString().trim().length();
    }
    
    private String replacePlaceholders(String... escapedStrings){
        String preparedStatement = this.getQuery();
        for(String escapedString : escapedStrings){
            preparedStatement = preparedStatement.replaceFirst("\\?", "'" + escapedString + "'");
        }
        return preparedStatement;
    }
    
    void populateReservedWords(){
        this.setAllowedKeywords(new ArrayList<>());
        String[] fileNames = {
                "SQLReservedWords.txt",
                "ODBCReservedWords.txt",
                "FutureReservedWords.txt"
        };
        for(String fileName : fileNames){
            try{
                File file = new File(this.classLoader.getResource(fileName).getFile());
                this.readFile(file);
            }catch(NullPointerException npex){
                LOGGER.warn("Could not find file '" + fileName + "'", npex);
            }catch(ArrayIndexOutOfBoundsException | IOException ex){
                LOGGER.warn("Could not read file '" + fileName + "', Caused By: ", ex);
            }
        }
    }
    
    void readFile(File file) throws FileNotFoundException {
        try(Scanner scanner = new Scanner(file)){
            while(scanner.hasNextLine()){
                String entry = scanner.nextLine();
                if(!this.allowedKeywords.contains(entry)){
                    this.addAllowedKeyword(entry);
                }
            }
        }catch(IOException ioex){
            LOGGER.warn("Could not read file '" + file.getName() + "'", ioex);
            throw ioex;
        }
    }
    
    private void addAllowedKeyword(String keyword){
        this.allowedKeywords.add(keyword);
    }
    
    private void setAllowedKeywords(List<String> keywords){
        this.allowedKeywords = keywords;
    }
    
    private String getQuery(){
        return this.query;
    }
    
    private void setQuery(String query){
        this.query = query;
    }
    
    private Integer findWords(String query, List<String> listOfWordsToFind){
        Integer timesFound = 0;
        for(String word : listOfWordsToFind){
            if(query.toUpperCase().contains(word.toUpperCase())
            && this.findOnWordBoundary(query.toUpperCase(), word.toUpperCase())){
                timesFound++;
            }
        }
        return timesFound;
    }
    
    private boolean findOnWordBoundary(String query, String wordToFind){
        String regex = "\\b" + wordToFind + "\\b";
        Pattern pattern = Pattern.compile(regex, Pattern.CASE_INSENSITIVE);
        Matcher matcher = pattern.matcher(query);
        return matcher.find();
    }
    
    private boolean validateValues(Object... values) throws InvalidValuesException{
        return this.validateValues(this.getQuery(), values);
    }
    
    /**
     * Validates the provided values in relation the original provided query.
     * @param query The original query string (with '?' as placeholders for values)
     * @param values An array of Object csv
     * @return True if valid, false if not
     */
    private boolean validateValues(String query, Object... values) throws InvalidValuesException{
        if(values.length == StringUtils.countOccurrencesOf(query, "?")){
            for(Object value : values){
                boolean validClass = false;
                for(Class clazz : this.allowedClasses){
                    if(value.getClass().equals(clazz)){
                        validClass = true;
                    }
                }
                if(!validClass){
                    throw new InvalidValuesException("Invalid Class");
                }
            }
            return true;
        }
        return false;
    }
    
    private void validateSQLSyntax(String query) throws OriginalQueryException{
        final JSqlParser jSqlParser = new CCJSqlParserManager();
        try{
            jSqlParser.parse(new StringReader(query));
        }catch(JSQLParserException e){
            throw new OriginalQueryException(String.format(
                    "Failed to Parse SQL: %s",
                    e.getMessage()
            ), e);
        }
    }
    
    private String[] escapeObjects(Object... values) throws InvalidValuesException{
        return this.escapeStrings(this.convertObjectsToStrings(values));
    }
    
    private String[] escapeStrings(String... values) throws InvalidValuesException{
        List<String> listToReturn = new ArrayList<>();
        for(String value: values){
            listToReturn.add(this.escapeString(value));
        }
        String[] stringsToReturn = new String[listToReturn.size()];
        stringsToReturn = listToReturn.toArray(stringsToReturn);
        return stringsToReturn;
    }
    
    private String escapeString(String value) throws InvalidValuesException{
        String stringToReturn = value;
        for(String prohibitedChar : this.prohibitedChars){
            stringToReturn = stringToReturn.replaceAll(prohibitedChar, "").trim();
        }
        for(String charToReplace : this.charsToReplace){
            stringToReturn = stringToReturn.replaceAll(charToReplace, "\\\\" + "\\" + charToReplace).trim();
        }
        if((this.findWords(value, this.allowedKeywords) > 0)
        || (this.findWords(value, this.prohibitedKeywords) > 0)){
            throw new InvalidValuesException(
                    "SQL Syntax Error: Found reserved words in provided value"
                    + "\n\t[QUERY]: " + this.getQuery()
                    + "\n\t[VALUE]: '" + value + "'"
                    + "\n\n"
            );
        }
        return stringToReturn;
    }
    
    private String[] convertObjectsToStrings(Object... values){
        List<String> stringValues = new ArrayList<>();
        for(Object value : values){
            for(Class clazz : this.allowedClasses){
                if(value.getClass().equals(clazz)){
                    stringValues.add(value.toString());
                }
            }
        }
        String[] stringsToReturn = new String[stringValues.size()];
        stringsToReturn = stringValues.toArray(stringsToReturn);
        return stringsToReturn;
    }
    
    public class OriginalQueryException extends SQLException{
        private OriginalQueryException(String message, Throwable cause){
            super(message, cause);
        }
    }
    
    public class InvalidValuesException extends SQLException{
        private InvalidValuesException(String message){
            super(message);
        }
    }
}
